/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */

import {workspace, ExtensionContext, window, commands} from 'vscode';
import {
  LanguageClient,
  LanguageClientOptions,
  ServerOptions,
  Transport,
  TransportKind,
  ProvideCompletionItemsSignature,
  DidChangeConfigurationNotification,
} from "vscode-languageclient/node";

import {TextDocument, CancellationToken, Position, CompletionContext} from "vscode";

let client: LanguageClient;

function startLSPClient() {
  // toggle this flag depending on whether your LSP server is a Node module or an executable
  const isServerExecutable = true;

  const transportOptions: Transport = {
    kind: TransportKind.socket,
    port: 6789
  };

  // Start the language server on specific port and communicate via socket connection
  let serverOptions: ServerOptions;

  if (isServerExecutable) {
    // If you have the executable, replace it with the path to your executable
    const command = '/Users/olenahoral/projects/LSP/bin/gitlab-lsp';

    serverOptions = {
      command,
      transport: transportOptions,
    };

  } else {
    // Runs the server in Node's Inspector mode so that VS Code or Chrome (or any other tool) can attach to the server for debugging
    const debugOptions = {
      execArgv: ["--nolazy", `--inspect=6010`],
    };

    // Replace this with the path to your module
    const module = '/Users/olenahoral/projects/LSP/out/node/main.js';

    serverOptions = {
      debug: {
        options: debugOptions,
        module,
        transport: transportOptions,
      },
      run: {
        module,
        transport: transportOptions,
      }
    };
  }

  let suggestionTrackingId;

  const workspaceFolder = workspace.workspaceFolders?.[0];

// Options to control the language client
  const clientOptions: LanguageClientOptions = {
    documentSelector: [
      {scheme: 'file', language: 'python'},
      {scheme: 'file', language: 'javascript'},
      {scheme: 'file', language: 'golang'},
    ],
    middleware: {
      provideCompletionItem: async (document: TextDocument,
                                    position: Position, context: CompletionContext,
                                    token: CancellationToken, next: ProvideCompletionItemsSignature) => {
        const temp_result: any = await next(document, position, context, token);
        if (temp_result?.length) {
          /*  extract trackingId for tracking
            code_suggestion_accepted, code_suggestion_rejected and code_suggestion_cancelled events*/
          const {data: {trackingId}} = temp_result[0];
          //store tracking id to track events at the suggestion lifecycle events happen
          suggestionTrackingId = trackingId;
        }
        return temp_result;
      },
      resolveCompletionItem: async (item) => {
        /* Track whether suggestion was accepted, rejected or cancelled. The real extension should decide
         when is the best point to track this and only one of the events can happen per suggestion.
         These notifications are added here for testing only. */
        ['suggestion_accepted', 'suggestion_cancelled', 'suggestion_rejected']
          .forEach((action) => {
            client.sendNotification('$/gitlab/telemetry', {
              category: 'code_suggestions',
              action,
              context: {
                trackingId: suggestionTrackingId,
              },
            });
          });

        return item;
      }
    },

    initializationOptions: {
      extension: {
        name: 'Test VS Code extension',
        version: '0.0.1',
      },
      ide: {
        name: 'VSCode',
        version: '1.81.1',
        vendor: 'Microsoft',
      },
    },
    // The VS Code extension supports only single workspace folder initially
    workspaceFolder,
  };

// Create the language client and start the client.
  client = new LanguageClient(
    'glabLSPTest',
    'GLab LSP Test',
    serverOptions,
    clientOptions
  );

// Start the client. This will also launch the server
  client.start()
    .then(() => {
      window.showInformationMessage('Started the Client');

      sendInstanceConfig();

      // the server notifies the client that token is invalid
      client.onNotification('$/gitlab/token/check', ({message, reason}) => {
        // Handle token refresh here. Once token is updated send the new token to the server.
        window.showErrorMessage(`Token check notification received: ${message}`);
      });
    })
    .catch((error) => {
      window.showErrorMessage(`Failed to start the Client: ${error}`);
    });
}

export function activate(context: ExtensionContext) {
  startLSPClient();

  commands.registerCommand("lsp-sample.restartLSPConnection", async function () {
    try {
      await client.stop();
    } catch (e) {
      console.error('Error stopping client');
    }

    startLSPClient();
  });

  workspace.onDidChangeConfiguration(e => {
    if (e.affectsConfiguration('lsp.gitlabToken')) {
      sendInstanceConfig();
    }
  });

  // The `vscode-languageclient` does not support `workspace/didChangeWorkspaceFolders` so we send it as a custom notification but conforming to the protocol
  const workspaceFolderMapper = ({name, uri, index}) => ({
    name,
    uri: uri.toString(),
    index
  });

  const workspaceFoldersChangeListener = workspace.onDidChangeWorkspaceFolders(({added, removed}) => {
    client.sendNotification('workspace/didChangeWorkspaceFolders', {
      added: added.map(workspaceFolderMapper),
      removed: removed.map(workspaceFolderMapper),
    });
  });

  context.subscriptions.push(workspaceFoldersChangeListener);
}

function sendInstanceConfig() {
  client.sendNotification(DidChangeConfigurationNotification.type, {
    // these settings should be taken from the user setting of the IDE
    settings: {
      token: 'glpat',
      baseUrl: 'https://gitlab.com',
      codeCompletion: {
        enableSecretRedaction: true,
      },
      telemetry: {
        trackingUrl: 'http://127.0.0.1:9091',
        actions: [
          { action: "suggestion_shown" },
        ]
      },
    }
  });
}

export function deactivate(): Thenable<void> | undefined {
  if (!client) {
    return undefined;
  }
  return client.stop();
}
