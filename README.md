# LSP Example

Heavily documented sample code for https://code.visualstudio.com/api/language-extensions/language-server-extension-guide

## Functionality

Implements the basic LSP client.
The client can start itself and the server and connect to the server
via socket connection. 
It can also send the token to the server. Though this might be changed.

The server code is located in a different repository https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/.


## Structure

```
.
├── client // Language Client
│   ├── src
│   │   ├── test // End to End tests for Language Client  - not ready yet
│   │   └── extension.ts // Language Client entry point
├── package.json // The extension manifest.
```

## Running the client

- Run `npm install` in this folder. This installs all necessary npm modules
- Open VS Code in this folder.
- Make sure to have the LSP server ready. [Check the next section for more details](#setting-up-the-server)
- Press Ctrl+Shift+B to start compiling the client in [watch mode](https://code.visualstudio.com/docs/editor/tasks#:~:text=The%20first%20entry%20executes,the%20HelloWorld.js%20file.).
- Switch to the Run and Debug View in the Sidebar (Ctrl+Shift+D).
- Select `Launch Client` from the drop down (if it is not already).
- Press ▷ to run the launch config (F5).
- In the [Extension Development Host](https://code.visualstudio.com/api/get-started/your-first-extension#:~:text=Then%2C%20inside%20the%20editor%2C%20press%20F5.%20This%20will%20compile%20and%20run%20the%20extension%20in%20a%20new%20Extension%20Development%20Host%20window.) instance of VSCode, create the javascript file and try to trigger autocomplete message from client to the server. E.g. type `console.` or `window.` - the list of the autocomplete options will show up. This is the moment when LSP server will run the `onCompletion` callback. You can start debugging the code suggestions functionality there.


## Setting up the server
- Clone [server repo](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/)
- Run `npm install`
- Compile the server code with `npm run compile`.
- If your server is a Node module:
  - Update the value of the `isServerExecutable` constant to `false` on line `26` of the `extension.ts`.
  - Copy the path to the main entrypoint of the `node` app (it should end with `out/node/main.js`)
  - Replace the value of the `module` constant on line 52 of the `extension.ts` in this repository with your path.
- If you server is an executable:
  - Update the value of the `isServerExecutable` constant to `true` on line `38` of the `extension.ts`.
  - Copy the path to your executable
  - Replace the value of the `command` constant on line 52 of the `extension.ts` in this repository with your path.
  
## Debugging the server
  Note that these instructions are valid only if your server is a Node module.
  - Open `chrome://inspect` in a Chrome browser. [More info](https://nodejs.org/en/docs/guides/debugging-getting-started#inspector-clients)
  - Click the `Configure` button and ensure your target host and port are listed - add `localhost:6010`. The port is set in `debugOptions` in the `extension.ts` file of this repo.
  - Click on the `Open dedicated DevTools for Node` link.
  - You do not need to start the server. The client (extension) starts it.
  - Put the `debugger` statement or add `console.log` in your server code.
  - Then when you launch the client the debugger will pause on the breakpoints.
